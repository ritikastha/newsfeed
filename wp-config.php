<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'newsfeed');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';*?q7i;#c}6gaEU.+&u<z(%Ko)~(WTY5+*tqU-O`e`]w6qx9nA@&{Q5jhm1G/c+F');
define('SECURE_AUTH_KEY',  '=LWuBT`ymo~+.5JryVnl[sL4=7v]A<X(?|4H*HB|IEniQ$[|`eejY_=y$Bm5we6o');
define('LOGGED_IN_KEY',    'VuL#b;2}7oj:Dv(fl%CL#J8^EXA.:mc&,H,|`W`N+y eSvMKU(WX>hd0tT(&6B4D');
define('NONCE_KEY',        'LKN%GaGEEn^c[S*`1;K](}Xh~/<D{96`H t,agPn0[2S%}<24A1?{feLN?qbMe0=');
define('AUTH_SALT',        'RO.ak9HIXrIMe}i4]>;TVHtVd^1_t>OVsS$jPc@^2g2:o.WuvR>-]0;6YFVZC2[T');
define('SECURE_AUTH_SALT', 'z!A4ZtWhAN8)ivZ?hOZ|GP[}3k~8<Hs,TRo#50ZadooQ418Y&[sPLo%m O}}]+sQ');
define('LOGGED_IN_SALT',   'azSfCv`s=)th3t~(W_8,z4!]o~T&r@29X(viF8~oh>k.Y?#zK9Gsg7Rf:IdRwJz_');
define('NONCE_SALT',       ')=yQ(6CDx@#K@tXezL>Cxgv]9WfFP&(I115wfBqx8*>#EX)J]?x24<Rc8nsVK{s#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
