<!DOCTYPE html>
<html>
<head>
    <title>NewsFeed</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/bootstrap.min.css">
<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo get_template_directory_uri()?><!--/css/font-awesome.min.css">-->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/theme.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="<?php echo get_site_url() ?>/js/html5shiv.min.js"></script>
    <script src="<?php echo get_site_url() ?>/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    <header id="header">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="header_top">
                    <div class="header_top_left">
                        <ul class="top_nav">
                            <?php
                            wp_nav_menu(array(
                                'theme_location'=>'header-menu',
                                'container'=> 'ul',
                                'menu_class'=> 'navbar-nav',
                                'container_id'=> 'nav-menu-container',
                                'fallback_cb'=> 'wp_bootstrap_navwalker::fallback',
                                'walker'=> new wp_bootstrap_navwalker(),
                            ));
                            ?>
                        </ul>
                    </div>
                    <div class="header_top_right">
                        <p><?php echo date(" l, F,j, Y") ;?> </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="header_bottom">
                    <div class="logo_area"><a href="<?php echo get_home_url();?>" class="logo"><img src="<?php the_field('header_logo','option')?>" alt=""></a></div>
                    <div class="add_banner"><a href="#"><img src="<?php the_field('advertisement_image')?>" alt=""></a></div>
                </div>
            </div>
        </div>
    </header>
    <section id="navArea">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav main_nav">
                    <li class="active"><a href="<?php echo get_home_url();?>"><span class="fa fa-home desktop-home"></span><span class="mobile-show">Home</span></a></li>
                    <?php
                                        wp_nav_menu(array(
                                            'theme_location'=>'header-menu1',
                                            'container'=> 'ul',
                                            'menu_class'=> 'navbar-nav',
                                            'container_id'=> 'nav-menu-container',
                                            'fallback_cb'=> 'wp_bootstrap_navwalker::fallback',
                                            'walker'=> new wp_bootstrap_navwalker(),
                                        ));
                                        ?>

<!--                    <li class="active"><a href="index.html"><span class="fa fa-home desktop-home"></span><span class="mobile-show">Home</span></a></li>-->
<!--                    --><?php
//                    wp_nav_menu(array(
//                        'theme_location'=>'header-menu1',
//                        'container'=> 'ul',
//                        'menu_class'=> 'navbar-nav',
//                        'container_id'=> 'nav-menu-container',
//                        'fallback_cb'=> 'wp_bootstrap_navwalker::fallback',
//                        'walker'=> new wp_bootstrap_navwalker(),
//                    ));
//                    ?>
                </ul>
            </div>
        </nav>
    </section>
    <section id="newsSection">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="latest_newsarea"> <span>Latest News</span>
                    <ul id="ticker01" class="news_sticker">
                        <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">My First News Item</a></li>
                        <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">My Second News Item</a></li>
                        <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">My Third News Item</a></li>
                        <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">My Four News Item</a></li>
                        <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">My Five News Item</a></li>
                        <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">My Six News Item</a></li>
                        <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">My Seven News Item</a></li>
                        <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">My Eight News Item</a></li>
                        <li><a href="#"><img src="images/news_thumbnail2.jpg" alt="">My Nine News Item</a></li>
                    </ul>
                    <div class="social_area">
                        <ul class="social_nav">
                            <li class="facebook"><a href="<?php the_field('fb_link','option')?>" target="_blank"></a></li>
                            <li class="twitter"><a href="<?php the_field('twitter_link','option')?>"target="_blank"></a></li>
                            <li class="googleplus"><a href="<?php the_field('google_plus__link','option')?>" target="_blank"></a></li>
                            <li class="youtube"><a href="<?php the_field('youtube_link','option')?>" target="_blank"></a></li>
                            <li class="mail"><a href="<?php the_field('mail_link','option')?>" target="_blank"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
