<footer id="footer">
    <div class="footer_top">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInLeftBig">
                   <a href=""><img src="<?php the_field('header_logo','option')?>"> </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInDown">
                    <h2>Tag</h2>
                    <ul class="tag_nav">
                        <?php
                        wp_nav_menu(array(
                            'theme_location'=>'footer-menu',
                            'container'=> 'ul',
                            'menu_class'=> 'navbar-nav',
                            'container_id'=> 'nav-menu-container',
                            'fallback_cb'=> 'wp_bootstrap_navwalker::fallback',

                        ));
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_widget wow fadeInRightBig">
                    <h2>Contact</h2>
                    <p><?php the_field('contact','option');?></p>
                    <address>
                       <?php the_field('phone_number','option');?>
                    </address>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <p class="copyright">Copyright &copy; 2045 <a href="index.html">NewsFeed</a></p>
        <p class="developer">Developed By Ritika Shrestha</p>
    </div>
</footer>
</div>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.li-scroller.1.0.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.newsTicker.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/custom.js"></script>
</body>
</html>