<?php
get_header();
?>


<section id="sliderSection">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="slick_slider">
                <?php
                if (have_rows('slider_repeater')):
                while (have_rows('slider_repeater')):the_row();
                ?>
                <div class="single_iteam"> <a href="pages/single_page.html">
                        <?php
                        $img=get_sub_field('slider_image');
                        if (!empty($img)):
                        ?><img src="<?php echo $img['url']?>" alt="<?php echo $img['alt']?>">
                            <?php endif; ?>
                            </a>
                    <div class="slider_article">
                        <h2><a class="slider_tittle" href="pages/single_page.html"><?php the_sub_field('slider_title')?></a></h2>
                        <p><?php the_sub_field('slider_content')?></p>
                    </div>
                </div>
                <?php
                endwhile;
                endif;
                ?>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="latest_post">
                <h2><span>Latest post</span></h2>
                <div class="latest_post_container">
                    <div id="prev-button"><i class="fa fa-chevron-up"></i></div>

                    <ul class="latest_postnav">
                        <?php
                        $blog= new WP_Query(array('post_type'=> 'post', 'posts_per_page'=> '6'));
                        if($blog->have_posts()):
                        while($blog->have_posts()):$blog->the_post();?>
                        <li>
                            <div class="media"> <a href="<?php the_permalink()?>" class="media-left"> <?php the_post_thumbnail();?></a>
                                <div class="media-body"> <a href="<?php the_permalink()?>" class="catg_title"> <?php the_title();?></a> </div>
                            </div>
                        </li>
                        <?php
                        wp_reset_postdata();
                        endwhile;
                        endif;

                        ?>
                    </ul>
                    <div id="next-button"><i class="fa  fa-chevron-down"></i></div>
                </div>
            </div>
        </div>
    </div>
</section>

    <section id="contentSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="left_content">
                    <div class="single_post_content">
                        <h2><span>Business</span></h2>
                            <?php
                            $i=1;
                            $ptype="post";
                            $the_query = new WP_Query( array( 'post_type'=>$ptype, 'posts_per_page' => '6','paged' => $paged, 'category_name'=>'BUSINESS'));
                            if ( $the_query->have_posts() ):
                                while ( $the_query->have_posts() ):
                                    $the_query->the_post();
                            if ($i==1):
                                    ?>
                        <div class="single_post_content_left">

                            <ul class="business_catgnav  wow fadeInDown">
                                <li>
                                    <figure class="bsbig_fig"> <a href="<?php the_permalink();?>" class="featured_img"> <?php the_post_thumbnail();?> <span class="overlay"></span> </a>
                                        <figcaption> <a href="<?php the_permalink();?>"><?php the_title();?></a> </figcaption>
                                     <?php the_excerpt();?>
                                    </figure>
                                </li>
                            </ul>
                        </div>
                            <?php $i++; else: ?>

                        <div class="single_post_content_right">
                            <ul class="spost_nav">
                                <li>
                                    <div class="media wow fadeInDown"> <a href="<?php the_permalink();?>" class="media-left"> <?php the_post_thumbnail(); ?></a>
                                        <div class="media-body"> <a href="<?php the_permalink();?>" class="catg_title"><?php the_title(); ?></a> </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                                    <?php
                                  $i++;
                                  endif;
                                    wp_reset_postdata();
                                endwhile;

                                endif;

                                    ?>
                    </div>
                    <div class="fashion_technology_area">
                        <div class="fashion">
                            <div class="single_post_content">
                                <h2><span>Laptops</span></h2>
                                    <?php
                                    $i=1;
                                    $ptype="post";
                                    $the_query = new WP_Query( array( 'post_type'=>$ptype, 'posts_per_page' => '6','paged' => $paged, 'category_name'=>'LAPTOPS'));
                                    if ( $the_query->have_posts() ):
                                        while ( $the_query->have_posts() ):
                                            $the_query->the_post();
                                            if ($i==1):
                                                ?>
                                <ul class="business_catgnav wow fadeInDown">
                                    <li>
                                        <figure class="bsbig_fig"> <a href="<?php the_permalink();?>" class="featured_img"> <?php the_post_thumbnail();?><span class="overlay"></span> </a>
                                            <figcaption> <a href="<?php the_permalink();?>"><?php the_title();?></a> </figcaption>
                                            <p><?php the_excerpt();?></p>
                                        </figure>
                                    </li>
                                </ul>
                                          <?php $i++; else:?>
                                <ul class="spost_nav">
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="<?php the_permalink();?>" class="media-left"> <?php the_post_thumbnail();?> </a>
                                            <div class="media-body"> <a href="<?php the_permalink();?>" class="catg_title"><?php the_title();?></a> </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                                                <?php
                                                $i++;
                                            endif;
                                                wp_reset_postdata();
                                            endwhile;
                                            endif;
                                            ?>
                        </div>
                        <div class="technology">
                            <div class="single_post_content">
                                <h2><span>Technology</span></h2>
                                <?php
                                $i=1;
                                $ptype="post";
                                $the_query = new WP_Query( array( 'post_type'=>$ptype, 'posts_per_page' => '6','paged' => $paged, 'category_name'=>'TECHNOLOGY'));
                                if ( $the_query->have_posts() ):
                                    while ( $the_query->have_posts() ):
                                        $the_query->the_post();
                                        if ($i==1):
                                            ?>
                                <ul class="business_catgnav">
                                    <li>
                                        <figure class="bsbig_fig wow fadeInDown"> <a href="<?php the_permalink();?>" class="featured_img"><?php the_post_thumbnail();?><span class="overlay"></span> </a>
                                            <figcaption> <a href="<?php the_permalink();?>"><?php the_title();?></a> </figcaption>
                                            <p><?php the_excerpt();?></p>
                                        </figure>
                                    </li>
                                </ul>
                                        <?php $i++; else:?>
                                <ul class="spost_nav">
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="<?php the_permalink();?>" class="media-left"> <?php the_post_thumbnail();?></a>
                                            <div class="media-body"> <a href="<?php the_permalink();?>" class="catg_title"><?php the_title();?></a> </div>
                                        </div>
                                    </li>
                                </ul>
                                            <?php
                                            $i++;
                                        endif;
                                        wp_reset_postdata();
                                        endwhile;
                                        endif;
                                            ?>
                            </div>
                        </div>
                    </div>
                    <div class="single_post_content">
                        <h2><span>Photography</span></h2>
                        <ul class="photograph_nav  wow fadeInDown">
                            <?php if (have_rows('photography_repeater')):
                                                        while (have_rows('photography_repeater')):the_row();
                                                        $img= get_sub_field('photography');

                                                        ?>
                                                        <li>
                                                            <div class="photo_grid">
                                                                <figure class="effect-layla"> <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $img['url']?>" title="<?php echo $img['alt']; ?>">
                                                                        <?php if (!empty($img));?>
                                                                            <img src="<?php echo $img['url']?>" alt="<?php echo $img['alt'];?>">
                                                                    </a> </figure>
                                                            </div>
                                                        </li>
                                                        <?php
                                                        endwhile;
                                                        endif;
                                                            ?>
                                                    </ul>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <aside class="right_content">
                    <div class="single_sidebar">
                        <h2><span>Popular Post</span></h2>
                        <ul class="spost_nav">
                            <li>
                                <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                                    <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                                </div>
                            </li>
                            <li>
                                <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                                    <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                                </div>
                            </li>
                            <li>
                                <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                                    <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                                </div>
                            </li>
                            <li>
                                <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                                    <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                                </div>
                            </li>
                        </ul>
                    </div>


                    <div class="single_sidebar wow fadeInDown">
                        <h2><span>Category Archive</span></h2>
                        <select class="catgArchive">
                            <option>Select Category</option>
                            <option>Life styles</option>
                            <option>Sports</option>
                            <option>Technology</option>
                            <option>Treads</option>
                        </select>
                    </div>

                </aside>
            </div>
        </div>
    </section>


<?php
get_footer();
?>